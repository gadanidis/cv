# My academic CV

The LaTeX code for my academic CV.
It's compiled using GitLab CI, which I set up following [this
guide](https://sayantangkhan.github.io/latex-gitlab-ci.html) by Sayantan Khan.
You can see the latest compiled PDF
[here](https://gitlab.com/gadanidis/cv/-/jobs/artifacts/master/raw/cv.pdf?job=compile_pdf).

Initially based on [Jason Blevins's
template](https://jblevins.org/projects/cv-template), but I've made significant
changes since then.
